#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies and greed

import random
import art_pball_logo
from os import system

system("clear")
print(art_pball_logo.logo)

def power_ball(color):
    if color == "white":
        p_ball = random.randint(1, 69)
    elif color == "red":
        p_ball = random.randint(1, 26)
    return p_ball

lines = int(input("How many lines? "))
system("clear")
print(art_pball_logo.logo)
line_nu = 0
for line in range(lines):
    line_nu += 1
    white_ball_numbers = []
    red_ball = power_ball("red")
    for white_ball in range(5):
        white_ball = power_ball("white")
        for ball in white_ball_numbers:
            while white_ball == ball:
                white_ball = power_ball("white")
        white_ball_numbers.append(white_ball)
    print(f"Line {line_nu}: {white_ball_numbers} : {red_ball}")
