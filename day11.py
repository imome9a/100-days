#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies

############### Blackjack Project #####################

#Difficulty Normal 😎: Use all Hints below to complete the project.
#Difficulty Hard 🤔: Use only Hints 1, 2, 3 to complete the project.
#Difficulty Extra Hard 😭: Only use Hints 1 & 2 to complete the project.
#Difficulty Expert 🤯: Only use Hint 1 to complete the project.

############### Our Blackjack House Rules #####################

## The deck is unlimited in size. 
## There are no jokers. 
## The Jack/Queen/King all count as 10.
## The the Ace can count as 11 or 1.
## Use the following list as the deck of cards:
cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
## The cards in the list have equal probability of being drawn.
## Cards are not removed from the deck as they are drawn.
## The computer is the dealer.

from art_bjack import logo
import random
import os

# functions
def deal_card():
    dealt_card = random.choice(cards)
    return dealt_card

# game start (initial hand)
play_game = input("Do you want to play a game of Blackjack? Type 'y' or 'n': ")
while play_game == 'y':
    os.system("clear")
    print(logo)
    # scores and cards tracker
    player_cards = []
    computer_cards = []
    player_score = 0
    computer_score = 0
    # evaluate ace
    ''' this could probably be a function as its too much duplication'''
    for deal in range(2):
        pcard = deal_card()
        if pcard == 11:
            if player_score <= 10:
                pcard = 11
            else:
                pcard = 1
        player_cards.append(pcard)
    dealer_card = deal_card()
    if dealer_card == 11:
        if computer_score <= 10:
            dcard = 11
        else:
            dcard = 1   
    computer_cards.append(dealer_card)
    ''' this could probably be a function as its too much duplication'''
    for score in player_cards:
        player_score += score
    for score in computer_cards:
        computer_score += score

    # game report
    print(f"   Your cards: {player_cards}, current score: {player_score}")
    print(f"   Computer's first card: {dealer_card}")

    # play cards (addtl cards)
    play_card = input("Type 'y' to get another card, type 'n' to pass: ")
    if play_card == "n":
        # run PC cards
        while computer_score < 17:
            dealer_new_card = deal_card()
            if dealer_new_card == 11:
                if computer_score <= 10:
                    dealer_new_card = 11
                else:
                    dealer_new_card = 1   
            computer_cards.append(dealer_new_card)
            computer_score = 0
            for value in computer_cards:
                computer_score += value
        
        # eval cards
        print(f"   Your final hand: {player_cards}, final score: {player_score}")
        print(f"   Computer's final hand: {computer_cards}, final score: {computer_score}")
        '''PC plays cards'''
        if player_score == 21:
            print("You got 21, You win!")
        elif computer_score > 21:
            print("Opponent went over. You win.")
        elif computer_score == player_score:
            print("Draw.")
        elif computer_score > player_score:
            print("You lose.")
        elif player_score > computer_score:
            print("You win.")

    while play_card == 'y':
        os.system("clear")
        print(logo)

        player_new_card = deal_card()
        if player_new_card == 11:
            if player_score <= 10:
                player_new_card = 11
            else:
                player_new_card = 1   
        player_cards.append(player_new_card)
        player_score = 0
        for value in player_cards:
            player_score += value
        
        # updated game report
        print(f"   Your cards: {player_cards}, current score: {player_score}")
        print(f"   Computer's first card: {computer_cards}, current score: {computer_score}")
        if player_score > 21:
            # run PC cards
            while computer_score < 17:
                dealer_new_card = deal_card()
                if dealer_new_card == 11:
                    if computer_score <= 10:
                        dealer_new_card = 11
                    else:
                        dealer_new_card = 1   
                computer_cards.append(dealer_new_card)
                computer_score = 0
                for value in computer_cards:
                    computer_score += value
            print(f"   Your final hand: {player_cards}, final score: {player_score}")
            print(f"   Computer's final hand: {computer_cards}, final score: {computer_score}")
            print(f"You went over. You lose.")
            break
        play_card = input("Type 'y' to get another card, type 'n' to pass: ")

        if play_card == "n":
            # run PC cards
            while computer_score < 17:
                dealer_new_card = deal_card()
                if dealer_new_card == 11:
                    if computer_score <= 10:
                        dealer_new_card = 11
                    else:
                        dealer_new_card = 1   
                computer_cards.append(dealer_new_card)
                computer_score = 0
                for value in computer_cards:
                    computer_score += value
            print(f"   Your final hand: {player_cards}, final score: {player_score}")
            print(f"   Computer's final hand: {computer_cards}, final score: {computer_score}")
            '''PC plays cards'''
            if player_score == 21:
                print("You got 21, You win!")
                break
            elif computer_score > 21:
                print("Opponent went over. You win.")
                break
            elif computer_score == player_score:
                print("Draw.")
                break
            elif computer_score > player_score:
                print("You lose.")
                break
            elif player_score > computer_score:
                print("You win.")
                break

    # ending eval
    play_game = input("Do you want to play a game of Blackjack? Type 'y' or 'n': ")

# end game
exit
