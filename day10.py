#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the "100 Days of Code: The Complete Python Pro Bootcamp for 2023" Udemy course

# --- project calc ---
import art_calc
import os

def add(n1, n2):
    """Adds two numbers together."""
    result = n1 + n2
    return result
def subtract(n1, n2):
    """Subtracts two numbers together."""
    result = n1 - n2
    return result
def multiply(n1, n2):
    """Multiplies two numbers together."""
    result = n1 * n2
    return result
def divide(n1, n2):
    """Divides two numbers together."""
    result = n1 / n2
    return result

def calculate(cal_n1, symbol, calc_n2):
    """Calculates a math formula based on inputs and operation symbol."""
    if symbol == "+":
        result = add(cal_n1, calc_n2)
        return result
    elif symbol == "-":
        result = subtract(cal_n1, calc_n2)
        return result
    elif symbol == "*":
        result = multiply(cal_n1, calc_n2)
        return result
    elif symbol == "/":
        result = divide(cal_n1, calc_n2)
        return result

operate = True
choice = "n"
# main logic
while operate:
    os.system('clear')
    print(art_calc.logo)

    if choice == "y":
        number = value
    else:
        number = float(input("What's the first number?: "))
    
    math_signs = {
        "add": "+",
        "subtract": "-",
        "multiply": "*",
        "divide": "/",
    }

    for key, value in math_signs.items():
        print(value)
    
    operation = input("Pick an operation: ")
    next_number = float(input("What's the next number?: "))
    value = calculate(number, operation, next_number)
    
    print(f"{number} {operation} {next_number} = {value}")
    choice = input(f"Type 'y' to continue with {value}, or type 'n' to start a new calculation: ")
