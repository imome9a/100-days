from turtle import Turtle, Screen
from color_pallet import pallet
from random import choice

# def rgb():
#     r = round(random.uniform(0.0, 1.0),2)
#     g = round(random.uniform(0.0, 1.0),2)
#     b = round(random.uniform(0.0, 1.0),2)
#     return r,g,b

screen = Screen()
screen.colormode(255)

raph = Turtle()
#raph.shape("arrow")
raph.hideturtle()
raph.up()
raph.setpos(-300,-300)
raph.down()

row_ct = 10
dots_per_row = 10
dot_spacing = 50

def reset():
    raph.up()
    raph.setheading(90)
    raph.fd(dot_spacing)
    raph.setheading(180)
    raph.fd(dot_spacing*dots_per_row)
    raph.setheading(0)
    raph.down()

for row in range(row_ct):
    for dot in range(dots_per_row):
        color = choice(pallet)
        #color = choice(pallet[randint(0,len(pallet)-1)]
        raph.dot(20,color)
        raph.up()
        raph.fd(dot_spacing)
        raph.down()
    reset()


screen.exitonclick()
