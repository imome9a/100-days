#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp 2023 Udemy
from turtle import Turtle, Screen
import random

leo = Turtle()
leo.shape("turtle")
leo.color("Green")

# square
# size = 100
# for turtle in range(4):
#     leo.forward(size)
#     leo.right(90)

# dashed line
# for t in range(10):
#     leo.forward(10)
#     leo.up()
#     leo.forward(10)
#     leo.down()

def rgb():
    r = round(random.uniform(0.0, 1.0),2)
    g = round(random.uniform(0.0, 1.0),2)
    b = round(random.uniform(0.0, 1.0),2)
    return r,g,b

# shapes (each is random color and each side is 100)
# sides = 2
# for pattern in range(8):
#     sides += 1
#     angle = 360 / sides
#     leo.pencolor(rgb())
#     for turns in range(sides):
#         leo.forward(100)
#         leo.right(angle)

# random walk
# def movement(paces,direction):
#     if direction == "n":
#         leo.left(90)
#         leo.forward(paces)
#     if direction == "s":
#         leo.right(90)
#         leo.forward(paces)
#     if direction == "e":
#         leo.forward(paces)
#     if direction == "w":
#         leo.right(180)
#         leo.forward(paces)

# turtle_movement = True
# while turtle_movement:
#     paces = 30
#     leo.speed("fastest")
#     leo.pensize(15)
#     leo.pencolor(rgb())
#     directions = ["n","s","e","w"]
#     choice = random.choice(directions)
#     movement(paces,choice)

# spirograph

turns = 300
for movement in range(turns):
    leo.speed("fastest")
    leo.pencolor(rgb())
    leo.circle(100)
    leo.right(3)


screen = Screen()
screen.exitonclick()