#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp for 2023, Udemy course

# --- project silent auction ---
import auction_art
import os

# initial artwork and welcome banner
print(auction_art.logo)
print("Welcome to the secret auction program.")

auction_db = []

# function to collect bidder information
def bidder(bidder_name, bidder_bid):
    auction_db.append({"name": bidder_name, "bid": bidder_bid})

resume_auction = True
# main logic to loop through program
while resume_auction:
    name = input("What is your name?: ")
    bid = int(input("What is your bid?: $"))
    others = input("Are there any other bidders? Type 'yes  or 'no'. ")
    bidder(name, bid)
    if others == "yes":
        os.system('clear')
        continue
    else: # end program and print desired results
        resume_auction = False
        highest_bid = 0
        highest_bidder = ""
        for entry in auction_db:
            if entry["bid"] > highest_bid:
                highest_bid = entry["bid"]
                highest_bidder = entry["name"]
        print(f"The winner is {highest_bidder} with a bid of ${highest_bid}.")