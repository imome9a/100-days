#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp for 2023, Udemy course

import random
import hangman_words
import hangman_art
import os

# variables
word_list = hangman_words.words# provided
chosen_word = random.choice(word_list)
list_chosen_word = list(chosen_word)
guess_list = []
end_of_game = False
lives = 6

# create empty list same length as chosen_word
display = []
for letter in chosen_word:
    display.append("_")

# game logic
os.system('clear')
while not end_of_game:
    # display info
    print(f"\n{hangman_art.logo}")
    print(f"# Lives left: {lives}")
    print(f"# Guesses you've already made: {' '.join(guess_list)}")
    print()
    print(f"{' '.join(display)}")
    print(hangman_art.stages[lives])

    # evaluate game ending scenarios
    if display == list_chosen_word:
        print(f"\nYou win!")
        end_of_game = True
    elif lives == 0:
        print(f"\nYou lose.")
        end_of_game = True
 
    # guess evaluation
    else:
        guess = input("\nGuess a letter: ").lower()
        os.system('clear')
        # check past guesses
        if guess in guess_list:
            print(f"** You've already guessed {guess}, try again. **")
        # check guess in word
        elif guess in chosen_word:
            position = 0
            for letter in chosen_word:
                if guess == letter:
                    display[position] = letter
                    position += 1 # could have used a range function for this loop
                else:
                    position += 1
            guess_list += guess
        else:
            guess_list += guess
            lives -= 1
            print(f"** You guessed {guess}, thats not in the word. You lose a life. **")
