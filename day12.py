#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the "100 Days of Code: The Complete Python Pro Bootcamp for 2023" Udemy course

from art_guess_game import logo
import random
# artwork
print(logo)
# global
answer = random.randrange(1, 100)
print("Welcome to the Number Guessing Game!")
print("I'm thinking of a number between 1 and 100.")
#print(f"Pssst, the correct answer is {answer}")
mode = input("Choose a difficulty. Type 'easy' or 'hard': ")
# game function
def guess(difficulty):
    if mode == "easy":
        attempts = 10
    elif mode == "hard":
        attempts = 5
    while attempts != 0:
        print(f"You have {attempts} attempts remaining to guess the number.")
        guess = int(input("Make a guess: "))
        if guess == answer:
            break
        elif guess > answer:
            print(f"Too high.\nGuess again.")
        elif guess < answer:
            print("Too low.\nGuess again.")
        attempts -= 1
    if guess == answer:
        print(f"You got it! The answer was {answer}")
    else:
        print("You've run out of guesses, you lose.")
# run game
guess(mode)