#!/usr/bin/env python
# Coded by Aaron Francis (aka imome9a)
# For funnsies per the Udemy course:
#    "100 Days of Code: The Complete Python Pro Bootcamp for 2023"
# Project: coffee machine program
# Reference: https://replit.com/@appbrewery/coffee-machine-final?embed=1&output=1#main.py

from os import system

# Provided:
MENU = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}

def coffee_report(): # this could have just been added to main body; no func needed
    '''Print current resource levels and money balance.'''
    water = resources["water"]
    milk = resources["milk"]
    coffee = resources["coffee"]
    print(f" Water: {water}ml\n Milk: {milk}ml\n Coffee: {coffee}g\n Money: ${'{:.2f}'.format(money)}")

money = float('{:.2f}'.format(round(0.00, 2)))
machine_state = True
system("clear")

while machine_state:
    brew = input("What would you like? (espresso/latte/cappuccino): ").lower()
    if brew == "off":
        print("Powering off.")
        break
    elif brew == "report":
        coffee_report()
    elif brew == "espresso" or brew == "latte" or brew == "cappuccino":
        if brew == "espresso":
            brew_milk = 0
        else:
            brew_milk = int(MENU[brew]["ingredients"]["milk"])
        brew_water = int(MENU[brew]["ingredients"]["water"])
        brew_coffee = int(MENU[brew]["ingredients"]["coffee"])
        brew_cost = float(MENU[brew]["cost"])
        available_water = int(resources["water"])
        available_milk = int(resources["milk"])
        available_coffee = int(resources["coffee"])
        #print(f"resources water:{available_water},milk:{available_milk},coffee:{available_coffee}")
        #print(f"brew water:{brew_water},milk:{brew_milk},coffee:{brew_coffee}")
        if available_water >= brew_water:
            if available_milk >= brew_milk:
                if available_coffee >= brew_coffee:
                    print("Please insert coins.")
                    quarters = int(input("How many quarters?: "))
                    dimes = int(input("How many dimes?: "))
                    nickles = int(input("How many nickles?: "))
                    pennies = int(input("How many pennies?: "))
                    total = float(round(((0.25*quarters)+(0.10*dimes)+(0.05*nickles)+(0.01*pennies)),2))
                    if total >= brew_cost:
                        money += brew_cost
                        diff = float(total - brew_cost)
                        resources["water"] -= brew_water
                        resources["milk"] -= brew_milk
                        resources["coffee"] -= brew_coffee
                        print(f"Here is ${'{:.2f}'.format(diff)} in change.")
                        print(f"Here is your {brew}. Enjoy!")
                    else:
                        print("Sorry that's not enough money. Money refunded.")
                else:
                    print("Sorry there is not enough coffee.")
            else:
                print("Sorry there is not enough milk.")
        else:
            print("Sorry there is not enough water.")
    else:
        print("Option not recognized, try again.")
