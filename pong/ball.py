# project: pong game

from turtle import Turtle
import random

# constants
movementList = ["upRight()", "downRight()", "upLeft()", "downLeft()"]
# startBall = random.choice(movementList)

class Ball(Turtle): # inherited class (from Turtle class)

    def __init__(self, position): # default executed method
        super().__init__() # ??
        self.shape("circle")
        self.color("white")
        self.penup()
        self.goto(position)
        self.x_move = 10
        self.y_move = 10
        
    def move(self): # move method
        new_x = self.xcor() + self.x_move 
        new_y = self.ycor() + self.y_move 
        self.goto(new_x, new_y)
    
    def bounce_y(self): # bounce method
        self.y_move *= -1

    def bounce_x(self): # bounce method
        self.x_move *= -1

    def reset_position(self): # bounce method
        self.goto(0,0)
        self.bounce_x()