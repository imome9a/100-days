# project: pong game

from turtle import Turtle

# constants
PADDLE_INCREMENTS = 20

class Paddle(Turtle): # inherited class (from Turtle class)

    def __init__(self, position): # default executed method
        super().__init__()
        self.shape("square")
        self.penup()
        self.seth(90)
        self.color("white")
        self.shapesize(stretch_wid=1, stretch_len=5, outline=None)
        self.goto(position)

    def up(self): # called method
        self.forward(PADDLE_INCREMENTS)
    
    def down(self): # called  method
        self.backward(PADDLE_INCREMENTS)
