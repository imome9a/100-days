# project: pong game

from turtle import Turtle

# constants
SCORE_ALIGN = "center"
SCORE_FONT = "Calibri"
SCORE_FONT_SIZE = 12


class Scoreboard(Turtle):
    def __init__(self, screen_height, screen_width):
        super().__init__()
        self.ht = screen_height
        self.wd = screen_width
        self.penup()
        self.hideturtle()
        self.color("white")
        self.left_points = 0
        self.right_points = 0
        self.update_left_sboard()
        self.update_right_sboard()
    
    def update_left_sboard(self):
        self.goto(-(self.wd / 4), ((self.ht / 2) - 30) )
        self.write(f"Left Paddle: {self.left_points}", False, align=SCORE_ALIGN, font=(SCORE_FONT, SCORE_FONT_SIZE, "bold"))

    def update_right_sboard(self):
        self.goto((self.wd / 4), ((self.ht / 2) - 30) )
        self.write(f"Right Paddle: {self.right_points}", False, align=SCORE_ALIGN, font=(SCORE_FONT, SCORE_FONT_SIZE, "bold"))

    def left_point(self):
        self.clear()
        self.left_points += 1
        self.update_left_sboard()
        self.update_right_sboard()

    def right_point(self):
        self.clear()
        self.right_points += 1
        self.update_right_sboard()
        self.update_left_sboard()

    def game_over(self):
        self.goto(0, 0)
        self.write("GAME OVER", False, align=SCORE_ALIGN, font=(SCORE_FONT, SCORE_FONT_SIZE, "bold"))