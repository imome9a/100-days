#!/usr/bin/env python
# auther: Aaron "imome9a" Francis
# purpose: for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp 2023 course
# project: pong game

#[x]TODO: create screen
#[x]TODO: create & move paddles
#[x]TODO: create & move ball
#[x]TODO: detect collision w/ wall & bounce
#[x]TODO: detect collision w/ paddle & bounce
#[x]TODO: detect when paddle misses
#[x]TODO: keep score

from turtle import Screen
from scoreboard import Scoreboard
from paddle import Paddle
from ball import Ball
import time

# constants
SCREEN_HEIGHT = 600 
SCREEN_WIDTH = 800
SCREEN_NUDGE = 20
TOP_WALL = int(SCREEN_HEIGHT / 2 - SCREEN_NUDGE) # y value
BOTTOM_WALL = int(-(SCREEN_HEIGHT / 2 - SCREEN_NUDGE)) # y value
LEFT_WALL = int(SCREEN_WIDTH / 2 - SCREEN_NUDGE) # x value
RIGHT_WALL = int(-(SCREEN_WIDTH / 2 - SCREEN_NUDGE)) # x value

# vars
sboard = Scoreboard(SCREEN_HEIGHT, SCREEN_WIDTH)
l_paddle = Paddle((-350, 0))
r_paddle = Paddle((350, 0))
ball = Ball((0, 0))

# display
screen = Screen()
screen.setup(width=SCREEN_WIDTH, height=SCREEN_HEIGHT)
screen.bgcolor("black")
screen.title("OME9A Pong")
screen.tracer(0)

# controls
screen.listen()
screen.onkey(key="w", fun=l_paddle.up)
screen.onkey(key="s", fun=l_paddle.down)
screen.onkey(key="Up", fun=r_paddle.up)
screen.onkey(key="Down", fun=r_paddle.down)

game_execute = True

while game_execute:
    time.sleep(0.1)
    screen.update()
    ball.move()

    # detect wall collisions
    if (ball.ycor() >= TOP_WALL) or (ball.ycor() <= BOTTOM_WALL):
        ball.bounce_y()

    # detect paddle collisions
    if (ball.distance(r_paddle) < 50 and ball.xcor() > 320) or (ball.distance(l_paddle) < 50 and ball.xcor() < -320):
        ball.bounce_x()

    # detect paddle misses
    if (ball.xcor() > 380) or (ball.xcor() < -380):
        if ball.xcor() > 380:
            sboard.left_point()
            ball.reset_position()
        if ball.xcor() < -380:
            sboard.right_point()
            ball.reset_position()

screen.exitonclick()