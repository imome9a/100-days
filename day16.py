#!/usr/bin/env python
# Coded by Aaron Francis (aka imome9a)
# For funnsies per the Udemy course:
#    "100 Days of Code: The Complete Python Pro Bootcamp for 2023"
# Project: OOP

#from turtle import Turtle, Screen
from prettytable import PrettyTable

# timmy = Turtle()
# timmy.shape("turtle")
# #timmy.color("red", "green")
# timmy.color("chartreuse3")
# timmy.forward(100)
# print(timmy)

# my_screen = Screen()
# my_screen.etableitonclick()
# print(my_screen.canvheight)

table = PrettyTable()
table.field_names = ["Pokemon Name", "Type"]
table.add_row(["Pikachu", "Electric"])
table.add_row(["Squirtle", "Water"])
table.add_row(["Charmander", "Fire"])
table.align = "l"

print(table)