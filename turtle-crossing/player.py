from turtle import Turtle

STARTING_POSITION = (0, -280)
MOVE_DISTANCE = 10
FINISH_LINE_Y = 280


class Player(Turtle): # inheriting the 'Turtle' class

    def __init__(self): # initialize the function automatically with below defined functions/vars
        super().__init__() # initialize the parent clas init function
        self.shape("turtle")
        self.penup()
        self.goto (STARTING_POSITION)
        self.setheading(90)

    def move(self):
        self.forward(MOVE_DISTANCE)