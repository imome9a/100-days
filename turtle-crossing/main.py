#!/usr/bin/env python
# auther: Aaron "imome9a" Francis
# purpose: for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp 2023 course
# project: turtle crossing

#[x]TODO: Move turtle with keypress - Create a turtle player that starts at the bottom of the screen and listen for the "Up" keypress to move the turtle north.
#[ ]TODO: Create and move cars - Create cars that are 20px high by 40px wide that are randomly generated along the y-axis and move to the left edge of the screen. No cars should be generated in the top and bottom 50px of the screen (think of it as a safe zone for our little turtle). Hint: generate a new car only every 6th time the game loop runs.
#[ ]TODO: Detect collision with car - Detect when the turtle player collides with a car and stop the game if this happens.
#[ ]TODO: Detect when turtle reaches other side - Detect when the turtle player has reached the top edge of the screen (i.e., reached the FINISH_LINE_Y). When this happens, return the turtle to the starting position and increase the speed of the cars. Hint: think about creating an attribute and using the MOVE_INCREMENT to increase the car speed.
#[ ]TODO: Create a scoreboard - Create a scoreboard that keeps track of which level the user is on. Every time the turtle player does a successful crossing, the level should increase. When the turtle hits a car, GAME OVER should be displayed in the centre.

import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

# display
screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)

# common
turtle = Player()

# controls
screen.listen()
screen.onkey(key="Up", fun=turtle.move)

game_is_on = True
while game_is_on:
    time.sleep(0.1)
    screen.update()

screen.exitonclick()