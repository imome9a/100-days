# Day 6 - Functions

def turn_right():
    turn_left()
    turn_left()
    turn_left()

def pivot():
    turn_right()
    if wall_in_front() == True:
        turn_right()
    else:
        move()

while not at_goal():
    if wall_in_front() == True:
        pivot()
    elif front_is_clear() == True:
        move()
    else:
        turn_left()
    move()