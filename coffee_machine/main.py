#!/usr/bin/env python
# Coded by Aaron Francis (aka imome9a)
# For funnsies per the Udemy course:
#    "100 Days of Code: The Complete Python Pro Bootcamp for 2023"
# Project: coffee machine program OOP

from menu import Menu
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine
from os import system

menu = Menu()
cmaker = CoffeeMaker()
money = MoneyMachine()

machine_state = True
system("clear")
while machine_state:
    brew_choice = input(f"What would you like? ({menu.get_items()}): ").lower()
    if brew_choice == "off":
        print("Powering off.")
        machine_state = False
    elif brew_choice == "report":
        cmaker.report()
        money.report()
    else: # check resources, check money, brew
        brew_item = menu.find_drink(brew_choice)
        if cmaker.is_resource_sufficient(brew_item):
            if money.make_payment(brew_item.cost):
                cmaker.make_coffee(brew_item)
