#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp 2023 course

# Turtle Race program
from turtle import Turtle, Screen
import random

tmnt_data = [
    {"name":"leonardo","color":"blue"},
    {"name":"raphael","color":"red"},
    {"name":"donatello","color":"purple"},
    {"name":"michelangelo","color":"orange"}
    ]

starting_positions = [
    {"x":-230,"y":75},
    {"x":-230,"y":25},
    {"x":-230,"y":-25},
    {"x":-230,"y":-75}
    ]

screen = Screen()
screen.setup(width=500, height=400)
place_bets = screen.textinput(title="Place your bets", prompt="Which TMNT "
    "will win the race? Leonardo, Raphael, Donatello, or Michelangelo?").lower()
tmnt_objects = []

for index, turtle in enumerate(tmnt_data):
    tmnt_name = turtle["name"]
    tmnt_name = Turtle(shape="turtle")
    tmnt_name.color(turtle["color"])
    tmnt_name.penup()
    tmnt_name.goto(x=starting_positions[index]["x"], y=starting_positions[index]["y"])
    tmnt_name.pendown()
    tmnt_objects.append(tmnt_name)

tmnt_race = True
while tmnt_race:
    for tmnt in tmnt_objects:
        if tmnt.xcor() >= 230:
            tmnt_race = False
            for each in tmnt_data:
                if tmnt.pencolor() == each["color"]:
                    tmnt_name = each["name"]
            print(f"{tmnt_name} won!")
            if tmnt_name == place_bets:
                print("Your TMNT won, congrats!")
            else:
                print("Your TMNT lost!")
        else:
            turtle_pace = random.randint(0, 10)
            tmnt.fd(turtle_pace)

screen.exitonclick()