#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp 2023 course

# Etch-A-Sketch program
from turtle import Turtle,Screen

raph = Turtle()
screen = Screen()

def move_fwd():
    raph.fd(5)

def move_bwd():
    raph.back(5)

def rotate_cclock():
    raph.right(5)

def rotate_clock():
    raph.left(5)

def clear_screen():
    #raph.clear()
    screen.reset()

screen.listen()
screen.onkey(key="w", fun=move_fwd)
screen.onkey(key="s", fun=move_bwd)
screen.onkey(key="a", fun=rotate_cclock)
screen.onkey(key="d", fun=rotate_clock)
screen.onkey(key="c", fun=clear_screen)
screen.exitonclick()