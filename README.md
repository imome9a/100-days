# 100 Days of Code (Python)
My "100 Days of Code: The Complete Python Pro Bootcamp for 2023" repo.

https://digitalu.udemy.com/course/100-days-of-code

# Concepts

# Day 1
- Loops
  - For
  - While

# Day 15
- Avoid modifying global scope within a function
  - Use CAPS for global scope constants to distinguish from global scope variables
  - Dont reuse a global scope variable name in a local capacity
  - use keyword "global" to identify a global variable inside a local scope

# Day 16
- OOP (Object Oriented Programming)
- Objects (two parts to an object, a. what is has, and b. what it does):
  - objects are copied or derived from a template (blueprint) which is ref to as the "class"
  - attributes (variables) (what it has)
  - function (what it does)
    - methods = (function) tied to an object (car.stop()) > "stop is method"

# Day 19
- learned to use enumerate() function to track index of an iterable (list) in a for loop

# Day 20
  - define a class with PascalCase
  - class Snake:
    ## this is the constructort which initializes (set to starting values) the class object
      def __init__(self, attrib1, attrib2): # 'self' is the thing being created or initialized
          # define starting values (i.e. attributes)
          self.attrib1 = attrib1 # my_snake = Snake(100,200), same as;
          self.attrib2 = attrib2 # my_snake.attrib1 = 100, my_snake.attrib2 = 200
      def something_else: # this is a 'method' (i.e. function) of the Snake class
          # define starting values (i.e. attributes)
          self.attrib1 = attrib1 # my_snake = Snake(100,200), same as;
          self.attrib2 = attrib2 # my_snake.attrib1 = 100, my_snake.attrib2 = 200

# Day 21
  - class inheritance
  - slicing a list [<nu>:<nu>]
