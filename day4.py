#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp for 2023, Udemy course

import random

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''
options = [rock, paper, scissors]

choose = int(input("What do you choose? Type 0 for Rock, 1 for Paper or 2 for Scissors\n"))
choice = (options[choose])
print(choice)

print("Computer chose:\n")
computer = random.choice(options)
print(computer)

if choice == computer:
    print("Tie")
elif (choice == rock) and (computer == scissors):
    print("You win")
elif (choice == paper) and (computer == rock):
    print("You win")
elif (choice == scissors) and (computer == paper):
    print("You win")
else:
    print("You lose")