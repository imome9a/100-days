from turtle import Turtle

SNAKE_MOVES = 20
SNAKE_ORIGIN = [(0,0), (-20,0), (-40,0)]

class Snake: # class

    def __init__(self): # init
        self.snake_size = 3
        self.snake_instance = []
        self.snake_create()
        self.head = self.snake_instance[0]

    def snake_create(self): # create a snake method
        for segment in SNAKE_ORIGIN:
            self.add_segment(segment)

    def add_segment(self, position): # extanded snake body method
        new_segment = Turtle(shape="square") 
        new_segment.penup()
        new_segment.color("white")
        new_segment.goto(position)
        self.snake_instance.append(new_segment)

    def extend(self): # extended snake body method 
        self.add_segment(self.snake_instance[-1].position())

    def move(self): # move method
        for segment in range(len(self.snake_instance) - 1, 0, -1):
            new_x = self.snake_instance[segment - 1].xcor()
            new_y = self.snake_instance[segment - 1].ycor()
            self.snake_instance[segment].goto(new_x, new_y)
        self.head.fd(SNAKE_MOVES)
 
    def up(self): # up method
        if self.head.heading() != 270:
            self.head.seth(90)
    
    def down(self): # down method
        if self.head.heading() != 90:
            self.head.seth(270)

    def left(self): # left method
        if self.head.heading() != 0:
            self.head.seth(180)

    def right(self): # right method
        if self.head.heading() != 180:
            self.head.seth(0)