from turtle import Turtle

# constants
SCORE_ALIGN = "center"
SCORE_FONT = "Calibri"
SCORE_FONT_SIZE = 12


class Scoreboard(Turtle):
    def __init__(self, screen_height):
        super().__init__()
        self.ht = screen_height
        self.penup()
        self.hideturtle()
        self.goto(0, ((self.ht / 2) - 30) )
        self.color("white")
        self.points = 0
        self.update_sboard()
    
    def update_sboard(self):
        self.write(f"Score: {self.points}", False, align=SCORE_ALIGN, font=(SCORE_FONT, SCORE_FONT_SIZE, "bold"))

    def point(self):
        self.clear()
        self.points += 1
        self.update_sboard()

    def game_over(self):
        self.goto(0, 0)
        self.write("GAME OVER", False, align=SCORE_ALIGN, font=(SCORE_FONT, SCORE_FONT_SIZE, "bold"))