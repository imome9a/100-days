#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp 2023 course

# Snake game
from turtle import Screen
from snake import Snake
from food import Food
from scoreboard import Scoreboard
import time

# constants
SCREEN_HEIGHT = 800
SCREEN_WIDTH = 800
TOP_WALL = int(SCREEN_HEIGHT / 2 - 10) # y value
BOTTOM_WALL = int(-(SCREEN_HEIGHT / 2 - 10)) # y value
LEFT_WALL = int(SCREEN_WIDTH / 2 - 10) # x value
RIGHT_WALL = int(-(SCREEN_WIDTH / 2 - 10)) # x value

snake = Snake()
food = Food(SCREEN_HEIGHT,SCREEN_WIDTH)
sboard = Scoreboard(SCREEN_HEIGHT)

screen = Screen()
screen.setup(width=SCREEN_WIDTH, height=SCREEN_HEIGHT)
screen.bgcolor("black")
screen.title("OME9A Snake")
screen.tracer(0)

screen.listen()
screen.onkey(key="w", fun=snake.up)
screen.onkey(key="s", fun=snake.down)
screen.onkey(key="a", fun=snake.left)
screen.onkey(key="d", fun=snake.right)

game_execute = True
while game_execute:
    screen.update()
    time.sleep(0.07)
    snake.move()
 
    # detect food consumption
    if snake.head.distance(food) < 16:
        sboard.point()
        snake.extend()
        food.move_food()

    # detect wall collision
    if snake.head.xcor() > LEFT_WALL or snake.head.xcor() < RIGHT_WALL:
        sboard.game_over()
        game_execute = False
    elif snake.head.ycor() > TOP_WALL or snake.head.ycor() < BOTTOM_WALL:
        sboard.game_over()
        game_execute = False

    # detect tail collision
    for segment in (snake.snake_instance[1:]):
        if snake.head.distance(segment) < 10:
            sboard.game_over()
            game_execute = False


screen.exitonclick()