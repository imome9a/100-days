from turtle import Turtle
import random
FOOD_WALL_SPACING = 15

class Food(Turtle):
    def __init__(self, screen_height, screen_width):
        super().__init__()
        self.screen_height = screen_height
        self.screen_width = screen_width
        self.shape("circle")
        self.penup()
        self.shapesize(stretch_len=0.6, stretch_wid=0.6)
        self.color("white")
        self.speed("fastest")
        self.move_food()

    def move_food(self):
        random_x = random.randint(-((self.screen_height / 2) - FOOD_WALL_SPACING), ((self.screen_height / 2) - FOOD_WALL_SPACING))
        random_y = random.randint(-((self.screen_width / 2) - FOOD_WALL_SPACING), ((self.screen_width / 2) - FOOD_WALL_SPACING))
        self.goto(random_x, random_y)
