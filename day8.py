#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp for 2023, Udemy course

# --- project Caesar Cipher ---
import art
print(art.logo)

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
ext_alphabet = alphabet+alphabet

def caesar(cypher_direction, cypher_text, cypher_shift):
    caesar_text = ""
    for char in cypher_text:
        if char in alphabet:
            index = alphabet.index(char)
            if cypher_direction == "encode":
                caesar_text += ext_alphabet[index+cypher_shift]
            elif cypher_direction == "decode":
                caesar_text += ext_alphabet[index-cypher_shift]
        else:
            caesar_text += char
    print(f'\nThe {cypher_direction}d text is: "{caesar_text}"')
    
run = True
while run:
    direction = input("\nType 'encode' to encrypt, type 'decode' to decrypt:\n").lower()
    text = input("\nType your message:\n").lower()
    shift = int(input("\nType the shift number:\n"))
    if shift > 25:
        print("\n**Your shift number is too high. Were going to use the maximum of 25**")
        shift = 25
    
    caesar(direction, text, shift)

    execute = input(f"\nType 'yes' if you want to go again, otherwise type 'no': ")
    if execute == "yes":
        continue
    else:
        print("\nGoodbye.")
        run = False
