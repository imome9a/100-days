#!/usr/bin/env python
# coded by Aaron Francis (aka imome9a)
# for funnsies per the 100 Days of Code: The Complete Python Pro Bootcamp 2023 course
from question_model import Question
from data import question_data, new_data
from quiz_brain import QuizBrain
# TODO: incorporate opentdb.com (OpenTrivia DB) data
question_bank = []
for question in question_data:
    data_text = question["text"]
    data_answer = question["answer"]
    data_question = Question(data_text, data_answer)
    question_bank.append(data_question)

quiz = QuizBrain(question_bank)

while quiz.still_has_questions():
    quiz.next_question()

print("\nYou finished!")
print(f"Your final score was: {quiz.score}/{len(quiz.question_list)}.")