class QuizBrain:

    def __init__(self, q_list):
        self.question_number = 0
        self.score = 0
        self.question_list = q_list
    
    def still_has_questions(self):
        return self.question_number < (len(self.question_list))
    
    def next_question(self):
        current_question = self.question_list[self.question_number]
        self.question_number += 1
        answer = input(f"\nQ.{self.question_number}: {current_question.text} (True/False)?: ").capitalize()
        self.check_answer(answer, current_question.answer)
    
    def check_answer(self, answer, correct_answer):
        if answer == correct_answer:
            self.score += 1
            print("You got it right!")
        else:
            print("Sorry you suck ass.")
        print(f"The correct answer was: {correct_answer}.")
        print(f"Your current score is: {self.score}/{self.question_number}.")