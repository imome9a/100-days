#!/usr/bin/env python
# Coded by Aaron Francis (aka imome9a)
# For funnsies per the Udemy course:
#    "100 Days of Code: The Complete Python Pro Bootcamp for 2023"
# Project: higher/lower game
# Reference: https://replit.com/@appbrewery/higher-lower-final?v=1

from art_high_low import logo
from art_high_low import vs
from data_high_low import data
from random import randint
from random import choice
from os import system

# def name_selector():
#     total_names = 0
#     for name in data:
#         total_names += 1
#     total_names = total_names - 1
#     name_int = randint(0, total_names)
#     name_selection = data[name_int]
#     name = name_selection["name"]
#     follower_count = name_selection["follower_count"]
#     description = name_selection["description"]
#     country = name_selection["country"]
#     return(name, follower_count, description, country)


def compare_names(follower_count_a, follower_count_b):
    '''Compare name A follower count to name B follower count.'''
    choice = input("Who has more follower_count? Type 'A' or 'B': ").lower()
    if (choice == "a") and (follower_count_a > follower_count_b):
        return True
    elif (choice == "b") and (follower_count_b > follower_count_a):
        return True
    else:
        return False


def game():
    system("clear")
    print(logo)
    score = 0
    name_a = choice(data)
    name_b = choice(data)
    # make sure selected names arent the same name
    while name_a == name_b:
            name_b = choice(data)
    while True:
        print(f"Compare A: {name_a['name']}, a {name_a['description']}, from {name_a['country']}")
        print(vs)
        print(f"Against B: {name_b['name']}, a {name_b['description']}, from {name_b['country']}")
        result = compare_names(name_a['follower_count'],name_b['follower_count'])
        if result:
            system("clear")
            score += 1
            print(logo)
            print(f"You're right! Current score: {score}.")
            name_a = name_b
            name_b = choice(data)
        else:
            system("clear")
            print(logo)
            print(f"Sorry, that's wrong. Final score: {score}")
            break


game()